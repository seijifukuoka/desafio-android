package br.com.seiji.desafioconcrete.webservice;

import br.com.seiji.desafioconcrete.models.Dribbble;
import br.com.seiji.desafioconcrete.models.DribbbleShot;
import retrofit.http.GET;

/**
 * Created by Seijownes on 21/05/2015.
 */
public interface DesafioInterface {

    @GET("/")
    public Dribbble listPopularDribble();

    @GET("/")
    public DribbbleShot getDribbleShot();
}

