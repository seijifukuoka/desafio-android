package br.com.seiji.desafioconcrete.activies;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spanned;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import br.com.seiji.desafioconcrete.R;
import br.com.seiji.desafioconcrete.models.DribbbleShot;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Seijownes on 21/05/2015.
 */
@EActivity(R.layout.detalhes_activity)
public class Detalhes extends Activity {

    protected boolean statusTwitter;
    protected boolean statusFacebook;
    protected boolean statusInstagram;
    protected boolean statusDribbble;
    @ViewById
    ImageView imgViewFull;
    @ViewById
    ImageView imgViewTwitter;
    @ViewById
    ImageView imgViewFacebook;
    @ViewById
    ImageView imgViewInstagram;
    @ViewById
    ImageView imgViewDribbble;
    @ViewById
    TextView tvText;
    @ViewById
    TextView tvViewCount;
    @ViewById
    RoundedImageView imgViewProfilePhoto;
    @ViewById
    TextView tvProfileName;
    @ViewById
    TextView tvDescricao;
    @Extra
    DribbbleShot dribbbleShotExtra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initialize() {
        ImageLoader.getInstance().displayImage(dribbbleShotExtra.getImage_url(), imgViewFull);
        tvText.setText(dribbbleShotExtra.getTitle());
        tvViewCount.setText(String.valueOf(dribbbleShotExtra.getViews_count()));
        ImageLoader.getInstance().displayImage(dribbbleShotExtra.getPlayer().getAvatar_url(), imgViewProfilePhoto);
        tvProfileName.setText(dribbbleShotExtra.getPlayer().getName());
        if (dribbbleShotExtra.getDescription() != null) {
            tvDescricao.setText(Html.fromHtml(dribbbleShotExtra.getDescription()));
        } else {
            tvDescricao.setText("");
        }

        imgViewTwitter.setImageDrawable(checkTwitter(!dribbbleShotExtra.isStatusTwitter()));
        imgViewFacebook.setImageDrawable(checkFacebook(!dribbbleShotExtra.isstatusFacebook()));
        imgViewInstagram.setImageDrawable(checkInstagram(!dribbbleShotExtra.isStatusInstagram()));
        imgViewDribbble.setImageDrawable(checkDribbble(!dribbbleShotExtra.isStatusDribbble()));
    }

    @Click(R.id.imgViewTwitter)
    protected void clickImgViewTwitter() {
        imgViewTwitter.setImageDrawable(checkTwitter(statusTwitter));

        if (statusTwitter) {
            Fabric.with(this, new TweetComposer());
            TweetComposer.Builder builder = new TweetComposer.Builder(this)
                    .text(this.getResources().getString(R.string.msg_Gostei_Deste_Dribbble) + "\n"
                            + dribbbleShotExtra.getTitle() + " - "
                            + dribbbleShotExtra.getShort_url());
            builder.show();
        }

    }

    @Click(R.id.imgViewFacebook)
    protected void clickimgViewFacebook() {
        imgViewFacebook.setImageDrawable(checkFacebook(statusFacebook));

        if (statusFacebook) {

            String messageString = " ";
            if (dribbbleShotExtra.getDescription() != null) {
                Spanned messageHtml = Html.fromHtml(dribbbleShotExtra.getDescription());
                messageString = messageHtml.toString();
            }

            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(dribbbleShotExtra.getShort_url()))
                    .setImageUrl(Uri.parse(dribbbleShotExtra.getImage_url()))
                    .setContentTitle(dribbbleShotExtra.getTitle())
                    .setContentDescription(messageString)
                    .build();

            ShareDialog.show(this, content);
        }
    }


    @Click(R.id.imgViewInstagram)
    protected void clickImgViewInstagram() {
        imgViewInstagram.setImageDrawable(checkInstagram(statusInstagram));
    }

    @Click(R.id.imgViewDribbble)
    protected void clickImgViewDribble() {
        imgViewDribbble.setImageDrawable(checkDribbble(statusDribbble));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    protected Drawable checkTwitter(boolean status) {

        Drawable drawable_off = ResourcesCompat.getDrawable(getResources(), R.drawable.twitter_off, null);
        Drawable drawable_on = ResourcesCompat.getDrawable(getResources(), R.drawable.twitter_on, null);
        Drawable drawable_return = drawable_on;

        if (status) {
            drawable_return = drawable_off;
            statusTwitter = false;
        } else {
            drawable_return = drawable_on;
            statusTwitter = true;
        }

        return drawable_return;
    }

    protected Drawable checkFacebook(boolean status) {

        Drawable drawable_off = ResourcesCompat.getDrawable(getResources(), R.drawable.facebook_off, null);
        Drawable drawable_on = ResourcesCompat.getDrawable(getResources(), R.drawable.facebook_on, null);
        Drawable drawable_return = drawable_on;

        if (status) {
            drawable_return = drawable_off;
            statusFacebook = false;
        } else {
            drawable_return = drawable_on;
            statusFacebook = true;
        }

        return drawable_return;
    }

    protected Drawable checkInstagram(boolean status) {

        Drawable drawable_off = ResourcesCompat.getDrawable(getResources(), R.drawable.instagram_off, null);
        Drawable drawable_on = ResourcesCompat.getDrawable(getResources(), R.drawable.instagram_on, null);
        Drawable drawable_return = drawable_on;

        if (status) {
            drawable_return = drawable_off;
            statusInstagram = false;
        } else {
            drawable_return = drawable_on;
            statusInstagram = true;
        }

        return drawable_return;
    }

    protected Drawable checkDribbble(boolean status) {

        Drawable drawable_off = ResourcesCompat.getDrawable(getResources(), R.drawable.dribble_icon_off, null);
        Drawable drawable_on = ResourcesCompat.getDrawable(getResources(), R.drawable.dribble_icon_on, null);
        Drawable drawable_return = drawable_on;

        if (status) {
            drawable_return = drawable_off;
            statusDribbble = false;
        } else {
            drawable_return = drawable_on;
            statusDribbble = true;
        }

        return drawable_return;
    }


}
