package br.com.seiji.desafioconcrete.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.androidannotations.annotations.EBean;

import java.util.List;

import br.com.seiji.desafioconcrete.R;
import br.com.seiji.desafioconcrete.models.DribbbleShot;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Seijownes on 21/05/2015.
 */

@EBean
public class ListAdapter extends ArrayAdapter<DribbbleShot> {

    private Context mContext;
    private Activity mActivity;
    private List<DribbbleShot> listShots;
    private LayoutInflater inflater;
    private boolean facebookStatus = false;
    private boolean instagramStatus = false;
    private boolean dribbbleStatus = false;

    public ListAdapter(Context context) {
        super(context, R.layout.item_list);
        mContext = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setAdapterActivity(Activity act) {
        mActivity = act;
    }

    public void setListAdater(List<DribbbleShot> listShotsPar) {
        listShots = listShotsPar;
    }

    @Override
    public int getCount() {
        return listShots.size();
    }

    @Override
    public DribbbleShot getItem(int position) {
        return listShots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_list, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        final DribbbleShot item = listShots.get(position);
        convertView.setMinimumHeight(parent.getMeasuredHeight() / 2);

        ImageLoader.getInstance().displayImage(item.getImage_url(), holder.imgViewFull, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            }
        });

        final Drawable drawable_twitter_on = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.twitter_on, null);
        final Drawable drawable_twitter_off = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.twitter_off, null);

        final Drawable drawable_facebook_on = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.facebook_on, null);
        final Drawable drawable_facebook_off = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.facebook_off, null);

        final Drawable drawable_instagram_on = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.instagram_on, null);
        final Drawable drawable_instagram_off = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.instagram_off, null);

        final Drawable drawable_dribbble_on = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.dribble_icon_on, null);
        final Drawable drawable_dribbble_off = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.dribble_icon_off, null);

        holder.tvText.setText(item.getTitle());
        holder.tvViewCount.setText(String.valueOf(item.getViews_count()));

        if (item.isStatusTwitter()) {
            holder.imgViewTwitter.setImageDrawable(drawable_twitter_on);
        } else {
            holder.imgViewTwitter.setImageDrawable(drawable_twitter_off);
        }

        if (item.isstatusFacebook()) {
            holder.imgViewFacebook.setImageDrawable(drawable_facebook_on);
        } else {
            holder.imgViewFacebook.setImageDrawable(drawable_facebook_off);
        }

        if (item.isStatusInstagram()) {
            holder.imgViewInstagram.setImageDrawable(drawable_instagram_on);
        } else {
            holder.imgViewInstagram.setImageDrawable(drawable_instagram_off);
        }

        if (item.isStatusDribbble()) {
            holder.imgViewDribbble.setImageDrawable(drawable_dribbble_on);
        } else {
            holder.imgViewDribbble.setImageDrawable(drawable_dribbble_off);
        }

        holder.imgViewTwitter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (item.isStatusTwitter()) {
                    holder.imgViewTwitter.setImageDrawable(drawable_twitter_off);
                    setTwitterStatusOff(position);
                } else {
                    holder.imgViewTwitter.setImageDrawable(drawable_twitter_on);
                    setTwitterStatusOn(position);

                    Fabric.with(mContext, new TweetComposer());
                    TweetComposer.Builder builder = new TweetComposer.Builder(mContext)
                            .image(Uri.parse(item.getImage_400_url()))
                            .text(mContext.getResources().getString(R.string.msg_Gostei_Deste_Dribbble) + "\n" + item.getTitle() + " - " + item.getShort_url());
                    builder.show();
                }
            }
        });

        holder.imgViewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (item.isstatusFacebook()) {
                    holder.imgViewFacebook.setImageDrawable(drawable_facebook_off);
                    setfacebookStatusOff(position);
                } else {
                    holder.imgViewFacebook.setImageDrawable(drawable_facebook_on);
                    setfacebookStatusOn(position);

                    String messageString = " ";
                    if (item.getDescription() != null) {
                        Spanned messageHtml = Html.fromHtml(item.getDescription());
                        messageString = messageHtml.toString();
                    }

                    ShareLinkContent content = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(item.getShort_url()))
                            .setImageUrl(Uri.parse(item.getImage_url()))
                            .setContentTitle(item.getTitle())
                            .setContentDescription(messageString)
                            .build();

                    ShareDialog.show(mActivity, content);
                }
            }
        });

        holder.imgViewInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.isStatusInstagram()) {
                    holder.imgViewInstagram.setImageDrawable(drawable_instagram_off);
                    setInstagramStatusOff(position);
                } else {
                    holder.imgViewInstagram.setImageDrawable(drawable_instagram_on);
                    setInstagramStatusOn(position);
                }
            }
        });

        holder.imgViewDribbble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.isStatusDribbble()) {
                    holder.imgViewDribbble.setImageDrawable(drawable_dribbble_off);
                    setDribbbleStatusOff(position);
                } else {
                    holder.imgViewDribbble.setImageDrawable(drawable_dribbble_on);
                    setDribbbleStatusOn(position);
                }
            }
        });

        return convertView;
    }

    public void setTwitterStatusOn(int position) {
        DribbbleShot shot = listShots.get(position);
        shot.setStatusTwitter(true);
        listShots.set(position, shot);
    }

    public void setTwitterStatusOff(int position) {
        DribbbleShot shot = listShots.get(position);
        shot.setStatusTwitter(false);
        listShots.set(position, shot);
    }

    public void setfacebookStatusOn(int position) {
        facebookStatus = true;
        listShots.get(position).setstatusFacebook(true);
        notifyDataSetChanged();
    }

    public void setfacebookStatusOff(int position) {
        facebookStatus = false;
        listShots.get(position).setstatusFacebook(false);
        notifyDataSetChanged();
    }

    public void setInstagramStatusOn(int position) {
        instagramStatus = true;
        listShots.get(position).setStatusInstagram(true);
        notifyDataSetChanged();
    }

    public void setInstagramStatusOff(int position) {
        instagramStatus = false;
        listShots.get(position).setStatusInstagram(false);
        notifyDataSetChanged();
    }

    public void setDribbbleStatusOn(int position) {
        dribbbleStatus = true;
        listShots.get(position).setStatusDribbble(true);
        notifyDataSetChanged();
    }

    public void setDribbbleStatusOff(int position) {
        dribbbleStatus = false;
        listShots.get(position).setStatusDribbble(false);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        private ImageView imgViewFull;
        private ImageView imgViewTwitter;
        private ImageView imgViewFacebook;
        private ImageView imgViewInstagram;
        private ImageView imgViewDribbble;
        private TextView tvText;
        private TextView tvViewCount;

        private ViewHolder(View view) {
            this.imgViewFull = (ImageView) view.findViewById(R.id.imgViewFull);
            this.imgViewTwitter = (ImageView) view.findViewById(R.id.imgViewTwitter);
            this.imgViewFacebook = (ImageView) view.findViewById(R.id.imgViewFacebook);
            this.imgViewInstagram = (ImageView) view.findViewById(R.id.imgViewInstagram);
            this.imgViewDribbble = (ImageView) view.findViewById(R.id.imgViewDribbble);
            this.tvText = (TextView) view.findViewById(R.id.tvText);
            this.tvViewCount = (TextView) view.findViewById(R.id.tvViewCount);
        }
    }

}
