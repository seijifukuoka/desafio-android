package br.com.seiji.desafioconcrete.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Seijownes on 21/05/2015.
 */
public class Dribbble {

    @SerializedName("page")
    private String page;

    @SerializedName("per_page")
    private int per_page;

    @SerializedName("pages")
    private int pages;

    @SerializedName("total")
    private int total;

    @SerializedName("shots")
    private List<DribbbleShot> shots;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DribbbleShot> getShots() {
        return shots;
    }

    public void setShots(List<DribbbleShot> shots) {
        this.shots = shots;
    }

    @Override
    public String toString() {
        return "Dribbble{" +
                "page='" + page + '\'' +
                ", per_page=" + per_page +
                ", pages=" + pages +
                ", total=" + total +
                ", shots=" + shots +
                '}';
    }
}
