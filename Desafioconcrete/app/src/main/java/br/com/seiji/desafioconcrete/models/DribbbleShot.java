package br.com.seiji.desafioconcrete.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Seijownes on 21/05/2015.
 */
public class DribbbleShot implements Serializable {

    private static final long serialVersionUID = -104137709256566564L;

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("height")
    private int height;

    @SerializedName("width")
    private int width;

    @SerializedName("likes_count")
    private int likes_count;

    @SerializedName("comments_count")
    private int comments_count;

    @SerializedName("rebounds_count")
    private int rebounds_count;

    @SerializedName("url")
    private String url;

    @SerializedName("short_url")
    private String short_url;

    @SerializedName("views_count")
    private int views_count;

    @SerializedName("rebound_source_id")
    private String rebound_source_id;

    @SerializedName("image_url")
    private String image_url;

    @SerializedName("image_teaser_url")
    private String image_teaser_url;

    @SerializedName("image_400_url")
    private String image_400_url;

    @SerializedName("player")
    private Player player;

    @SerializedName("created_at")
    private String created_at;

    private boolean statusTwitter = false;
    private boolean statusFacebook = false;
    private boolean statusInstagram = false;
    private boolean statusDribbble = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public int getRebounds_count() {
        return rebounds_count;
    }

    public void setRebounds_count(int rebounds_count) {
        this.rebounds_count = rebounds_count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShort_url() {
        return short_url;
    }

    public void setShort_url(String short_url) {
        this.short_url = short_url;
    }

    public int getViews_count() {
        return views_count;
    }

    public void setViews_count(int views_count) {
        this.views_count = views_count;
    }

    public String getRebound_source_id() {
        return rebound_source_id;
    }

    public void setRebound_source_id(String rebound_source_id) {
        this.rebound_source_id = rebound_source_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getImage_teaser_url() {
        return image_teaser_url;
    }

    public void setImage_teaser_url(String image_teaser_url) {
        this.image_teaser_url = image_teaser_url;
    }

    public String getImage_400_url() {
        return image_400_url;
    }

    public void setImage_400_url(String image_400_url) {
        this.image_400_url = image_400_url;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public boolean isStatusTwitter() {
        return statusTwitter;
    }

    public void setStatusTwitter(boolean statusTwitter) {
        this.statusTwitter = statusTwitter;
    }

    public boolean isstatusFacebook() {
        return statusFacebook;
    }

    public void setstatusFacebook(boolean statusFacebook) {
        this.statusFacebook = statusFacebook;
    }

    public boolean isStatusInstagram() {
        return statusInstagram;
    }

    public void setStatusInstagram(boolean statusInstagram) {
        this.statusInstagram = statusInstagram;
    }

    public boolean isStatusDribbble() {
        return statusDribbble;
    }

    public void setStatusDribbble(boolean statusDribbble) {
        this.statusDribbble = statusDribbble;
    }

    @Override
    public String toString() {
        return "DribbbleShot{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", likes_count=" + likes_count +
                ", comments_count=" + comments_count +
                ", rebounds_count=" + rebounds_count +
                ", url='" + url + '\'' +
                ", short_url='" + short_url + '\'' +
                ", views_count=" + views_count +
                ", rebound_source_id='" + rebound_source_id + '\'' +
                ", image_url='" + image_url + '\'' +
                ", image_teaser_url='" + image_teaser_url + '\'' +
                ", image_400_url='" + image_400_url + '\'' +
                ", player=" + player +
                ", created_at='" + created_at + '\'' +
                ", statusTwitter=" + statusTwitter +
                ", statusFacebook=" + statusFacebook +
                ", statusInstagram=" + statusInstagram +
                ", statusDribbble=" + statusDribbble +
                '}';
    }
}
