package br.com.seiji.desafioconcrete.activies;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.seiji.desafioconcrete.R;
import br.com.seiji.desafioconcrete.adapter.ListAdapter;
import br.com.seiji.desafioconcrete.models.Dribbble;
import br.com.seiji.desafioconcrete.models.DribbbleShot;
import br.com.seiji.desafioconcrete.webservice.RestHelper;


@EActivity(R.layout.main_activity)
public class MainActivity extends Activity implements AbsListView.OnScrollListener {

    protected Dribbble dribbblePopular;
    protected DribbbleShot dribbbleShot;
    protected List<DribbbleShot> listShots;
    protected int pageNumber = 1;
    protected int m_PreviousTotalCount = 0;
    @ViewById
    ListView lvMain;
    @Bean
    ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listShots = new ArrayList<>();
        listAdapter = new ListAdapter(this);

        buscarDribblePopular();
    }

    @AfterViews
    void initialize() {
        lvMain.setOnScrollListener(this);
    }

    @UiThread
    void setarAdapter() {
        if (pageNumber == 2) {
            listAdapter.setListAdater(listShots);
            listAdapter.setAdapterActivity(this);
            listAdapter.notifyDataSetChanged();
            lvMain.setAdapter(listAdapter);
        } else {
            listAdapter.setListAdater(listShots);
            listAdapter.setAdapterActivity(this);
            listAdapter.notifyDataSetChanged();
        }
    }

    @ItemClick
    void lvMainItemClicked(DribbbleShot shot) {
        Detalhes_.intent(this).dribbbleShotExtra(shot)
                .start();
    }

    @Background
    protected void buscarDribblePopular() {
        dribbblePopular = new Dribbble();
        RestHelper restHelp = new RestHelper();
        restHelp.getHostAdapter("http://api.dribbble.com/shots/popular?page=" + String.valueOf(pageNumber));
        dribbblePopular = restHelp.getApiService().listPopularDribble();
        pageNumber++;

        listShots.addAll(dribbblePopular.getShots());
        setarAdapter();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount == 0 || listAdapter == null)
            return;

        if (m_PreviousTotalCount == totalItemCount)
            return;

        boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

        if (loadMore) {
            m_PreviousTotalCount = totalItemCount;
            buscarDribblePopular();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(getResources().getString(
                R.string.fixo_Atencao));
        alertDialogBuilder
                .setMessage(
                        getResources().getString(R.string.msg_Sair_Aplicativo))
                .setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.fixo_Sim),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                .setPositiveButton(getResources().getString(R.string.fixo_Nao),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
