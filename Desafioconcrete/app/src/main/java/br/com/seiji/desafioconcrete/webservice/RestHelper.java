package br.com.seiji.desafioconcrete.webservice;

import retrofit.RestAdapter;

/**
 * Created by Seijownes on 21/05/2015.
 */
public class RestHelper {

    private static final String BASE_URL = "http://api.dribbble.com/shots/";
    private DesafioInterface desafioInterface;

//    public RestHelper() {
//        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .setEndpoint(BASE_URL).build();
//        desafioInterface = restAdapter.create(DesafioInterface.class);
//    }

    public RestAdapter getHostAdapter(String baseHost) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(baseHost).setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        desafioInterface = restAdapter.create(DesafioInterface.class);
        return restAdapter;
    }

    public DesafioInterface getApiService() {
        return desafioInterface;
    }

}
