package br.com.seiji.desafioconcrete;

import android.app.Application;
import android.graphics.Bitmap;

import com.facebook.FacebookSdk;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.File;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Seijownes on 21/05/2015.
 */
public class DesafioApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "tnyPHMtSYiD6S6iAFPOea2UuN";
    private static final String TWITTER_SECRET = "cDIx32BH9lVXc2KqoQ2q2e11rKxhhzv0JPIR54mMusheQKvgT1";


    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        initImageLoader();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }


    private void initImageLoader() {
        File cacheDir = StorageUtils.getCacheDirectory(this);

        DisplayImageOptions.Builder dio = new DisplayImageOptions.Builder();
        dio.cacheInMemory(true);
        dio.cacheOnDisk(true);
        dio.imageScaleType(ImageScaleType.EXACTLY);
        dio.bitmapConfig(Bitmap.Config.RGB_565);
        DisplayImageOptions opts = dio.build();

        ImageLoaderConfiguration.Builder ilc = new ImageLoaderConfiguration.Builder(getApplicationContext());
        ilc.threadPoolSize(5);
        ilc.threadPriority(Thread.NORM_PRIORITY - 2);
        ilc.tasksProcessingOrder(QueueProcessingType.FIFO);
        ilc.denyCacheImageMultipleSizesInMemory();
        ilc.memoryCache(new LruMemoryCache(2 * 1024 * 1024));
        ilc.memoryCacheSizePercentage(13);
        ilc.diskCacheExtraOptions(480, 320, null);
        ilc.diskCache(new UnlimitedDiscCache(cacheDir));
        ilc.diskCacheFileCount(100);
        ilc.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        ilc.imageDownloader(new BaseImageDownloader(this));
        ilc.defaultDisplayImageOptions(opts);
        ImageLoaderConfiguration config = ilc.build();

        ImageLoader.getInstance().init(config);
    }
}

